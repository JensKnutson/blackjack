
/*Övning blackjack
 * Blackjack-klassen ska kunna dra kort och kunna 
 * räkna ut spelarens poäng.
Av Chi och Jens
*/

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// Skapa en ny instance av blackjack klassen
		Blackjack bj = new Blackjack();

		// BLanda kortleken
		bj.shuffleDeck();

		// Skriv ut alternativen till användaren
		System.out.println("1 för hit \n2 för stand \n3 för reset \n4 för att avsluta");
		System.out.println("-----------------------------------");
		// Instanciera scanner kalssen
		Scanner sc = new Scanner(System.in);

		// Dra första handen innan vi går in i spelet, två kort dras
		bj.starterHand();
		
		//While sats för att hålla igång loopen och switch sats för att göra valen
		while (true) {
			switch (sc.nextInt()) {
			case 1:
				bj.hit();
				break;
			case 2:
				bj.stand();
				break;
			case 3:
				bj.reset();
				break;
			case 4:
				System.out.println("Tack för du spelade");
				sc.close();
				return;
			default:
				System.out.println("Ange ett giltligt värde");
				break;
			}
		}
	}
}
