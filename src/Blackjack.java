import java.util.ArrayList;

public class Blackjack {
	//En arraylist som håller kolla på vilka kort användaren har på handen
	ArrayList<Card> hand = new ArrayList<Card>();
	Deck deck = new Deck();
	
	//Score håller koll på använadrens poäng
	private int score;
	private int git;
	
	//drawnCards håller koll på hur långt in i kortleken vi kommit
	private int drawnCards = 0;

	
	//Blandar kortleken med metoden från deck klassen
	public void shuffleDeck() {
		deck.shuffle();
	}

	
	public void hit() {
		//Lägg till nästa kort i handen
		hand.add(deck.draw(drawnCards));
		//Score metoden retunerar värdet på kortet vi har i handen
		int score = score();
		
		//Lägg till kortets värde i klass variablen
		this.score += score;
		
		//Skriv ut meddelande till användaren med print funktionen
		print();
		
		//Öka på drawncards för att veta vart vi är i kortleken
		this.drawnCards++;
		
		//Kolla om användaren fick över 21 med bust metoden
		bust();
		
		//kolla om användaren fick 21 och att det inte var användarens starthand. Starta om spelet
		if (maxScore(this.score) && hand.size() > 2) {
			System.out.println("Du fick 21!");
			reset();
		}
	}

	public void print() {
		//Hantera utskriften, skriv ut vilket klätt kort du fick istället för värdet den har
		String s1;
		if (deck.deck.get(drawnCards).getValue() == 11) {
			s1 = "Jack";
		} else if (deck.deck.get(drawnCards).getValue() == 12) {
			s1 = "Queen";
		} else if (deck.deck.get(drawnCards).getValue() == 13) {
			s1 = "King";
		} else if (deck.deck.get(drawnCards).getValue() == 1) {
			s1 = "Ace";
		} else
			s1 = Integer.toString(deck.deck.get(drawnCards).getValue());
		System.out.println("Du fick " + s1 + " of " + deck.deck.get(drawnCards).getSuit() + "\n(Din poäng är " + this.score + ")");
	}

	public int scoreCourtCards(int cardValue) {
		//Retunera 10 för jack, dam och kung enligt blackjack regler
		if (cardValue > 10) {
			return 10;
			//Kolla vilket ess som ger bäst resutlat med en annan metod
		} else if (cardValue == 1) {
			return checkAceForblackjack();
		} else
			//Retunera annars värden mellan 2-10
			return cardValue;
	}
	
	public int checkAceForblackjack(){
		//Räkna ess som 11 om användaren inte går över 21. Om användaren går över 21 så räknas ess som 1
		if(this.score + 11 > 21) {
			return 1;
		}
		else return 11;
	}
	
	public void bust() {
//		Kolla om användaren kom över 21 och förlorar
		if (this.score > 21) {
			System.out.println("Du kom över 21");
			reset();
		}
	}

	public void stand() {
		//Starta om spelet om användaren väljar att stanna. Här skulle dealern hoppa in om vi hade haft en
		System.out.println("Du fick " + this.score + " poäng");
		System.out.println("Nytt spel startas");
		reset();
	}

	private int score() {
		//Retunera värdet för kortet vi har dragit till hand arrayen
		int score = 0;
		score = hand.get(this.drawnCards).getValue();
		score = scoreCourtCards(score);
		return score;
	}

	public boolean maxScore(int score) {
		//Kolla om man fått 21
		if (score == 21) {
			return true;
		} else
			return false;

	}

	public void reset() {
		//Återställ spelet, sätt alla variabler till 0, skapa nytt deck, blanda det och dra två nya kort
		this.drawnCards = 0;
		this.score = 0;
		this.hand.clear();
		deck = new Deck();
		deck.shuffle();
		System.out.println("-----------------------------------");
		starterHand();

	}

	public void starterHand() {
		//Skriv ut meddelande och dra två kort
		System.out.println("\nDin starthand är: ");
		hit();
		hit();
		
		//Kolla om användaren fick blackjack och starat om i så fall
		if (this.score == 21) {
			System.out.print("Du fick Blackjack! \n");
			reset();
		}
	}
}
